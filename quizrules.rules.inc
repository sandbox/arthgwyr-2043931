<?php

/**
 * @file
 * Quiz related conditions and actions for Rules.
 */

/**
 * Implements hook_rules_event_info().
 */
function quizrules_rules_event_info() {
  return array(
    'quizrules_quiz_taken' => array(
      'group' => t('QuizRules'),
      'label' => t('User has taken a quiz'),
      'module' => 'quiz',
      'variables' => array(
        'quiz' => array('type' => 'quiz', 'label' => t('Quiz')),
        'quizresult' => array('type' => 'quiz_result', 'label' => t('Quiz Result')),
      ),
    ),
  );
}

/**
 * Implements hook_rules_condition_info().
 */
function quizrules_rules_condition_info() {
  $items = array();

  $items['quizrules_condition_quiz'] = array(
    'label' => t('Quiz'),
    'module' => 'quizrules',
    'group' => 'QuizRules',
    'parameter' => array(
      'quizid' => array(
        'type' => 'text',
        'label' => t('Quiz'),
        'options list' => 'quizrules_get_quizlist',
      ),
    ),
  );

  $items['quizrules_condition_answer'] = array(
    'label' => t('Answers'),
    'module' => 'quizrules',
    'group' => 'QuizRules',
    'parameter' => array(
      'questionid' => array(
        'type' => 'text',
        'label' => t('Questions'),
        'options list' => 'quizrules_get_questions',
      ),
      'answerid' => array(
        'type' => 'text',
        'label' => t('Answers'),
        'options list' => 'quizrules_get_answers',
      ),
    ),
    'callbacks' => array('form_alter' => 'quizrules_question_select'),
  );

  return $items;
}

/**
 * Implements hook_rules_data_info().
 */
function quizrules_rules_data_info() {
  return array(
    'quiz_result' => array(
      'label' => t('Quiz result'),
      'wrap' => FALSE,
      'group' => t('Quiz Rules'),
    ),
    'quiz' => array(
      'label' => t('Quiz'),
      'wrap' => FALSE,
      'group' => t('Quiz Rules'),
    ),
  );
}

/**
 * A function to check0 the quiz.
 */
function quizrules_condition_quiz($quizid) {
  if ($node = menu_get_object()) {
    if ($node->type == 'quiz' && $node->nid == $quizid) {
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * A function to test the response.
 */
function quizrules_condition_answer($questionid, $answerid, $array_params, $rules_state) {
  if (isset($rules_state->variables['quizresult'])) {
    foreach ($rules_state->variables['quizresult'] as $qid => $answers) {
      foreach ($answers as $aid) {
        if ($aid == $answerid && $qid == $questionid) {
          return TRUE;
        }
      }
    }
  }

  return FALSE;
}
